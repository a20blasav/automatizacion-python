import os, zipfile

def backupToZip(folder):
"""Fai unha copia de seguridade da carpeta que lle pasemos, e comprímea nun zip

"""

    folder = os.path.abspath(folder)

    number = 1
    while True:
        zipfileName = os.path.basename(folder) + '_' + str(number)+'.zip'
        if not os.path.exists(zipfileName):
            break
        number = number+1

    backupZip = zipfile.ZipFile(zipfileName, 'w')

    for foldername, subfolders, filenames in os.walk(folder):
        backupZip.write(foldername)
        for filename in filenames:
            backupZip.write(os.path.join(foldername, filename))
        backupZip.close()
        print("Archivo "+ zipfileName +" creado. Fin del proceso")
