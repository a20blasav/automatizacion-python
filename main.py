import requests
from backup import backupToZip


def proc_data(d):
    """Función que procesa os datos delimitados por "|" e devolve unha lista con eles eliminando o separador
    :param d: Os datos que se lle pasan, delimitados por "|"
    :return: devolve unha lista cos datos
    """
    resultado = None

    if d is not None:
        datos = d.split('|')
        operador = datos[0]
        comando1 = datos[1]
        comando2 = datos[2]
        comando3 = datos[3]
        resultado = operador, comando1, comando2, comando3
        return (resultado)


if __name__ == '__main__':
    url = "https://gitlab.com/a20blasav/automatizacion-python/-/raw/main/flow.txt"
    res = requests.get(url)
    datos=proc_data(res.text)
    isBkp = datos[1].split(':')
    isAuth = datos[3].split(':')

    if int(isBkp[1]) :
        if int(isAuth[1]):
            backupToZip("proba")
        else:
            print("Hubo un error en la autenticación")



